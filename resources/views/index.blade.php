<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{ env('APP_NAME') }}</title>
    <link href=" {{ mix('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('assets/fa/css/all.css') }}">
</head>
<style>
    .bookmarkRibbon {
        width: 16px;
        height: 0;
        border-bottom: 6px solid #045089;
        border-top: 6px solid #045089;
        border-left: 5px solid transparent;
        margin-bottom: 6px;
        font-size: 5.5px;
        font-weight: 224;
        position: absolute;
        top: 0;
        transform: rotate(-90deg)
    }

    .shape {
        width: 0px;
        height: 150px;
        box-sizing: border-box;
        border: 1px solid #000;
        border-bottom: 25px solid transparent;
        border-top: none;
        border-left: 50px solid #096ae8;
        border-right: 50px solid #096ae8;
    }

    .bookmark {
        position: relative;
    }

    @font-face {
        font-family: roboto;
        src: url("{{ url('assets/fonts/roboto/Roboto-Regular.ttf') }}");
    }

    @font-face {
        font-family: roboto-light;
        src: url("{{ asset('assets/fonts/roboto/Roboto-Light.ttf') }}");
    }


    @font-face {
        font-family: montserrate;
        src: url("{{ asset('assets/fonts/montserrat/Montserrat-Bold.otf') }}");
    }

    @font-face {
        font-family: montserrate-regular;
        src: url("{{ asset('assets/fonts/montserrat/Montserrat-Regular.otf') }}");
    }

    @font-face {
        font-family: montserrate-light;
        src: url("{{ asset('assets/fonts/montserrat/Montserrat-Light.otf') }}");
    }




    .bookmark:before {
        content: "\f02e";
        /* this is your text. You can also use UTF-8 character codes as I do here */
        font-family: FontAwesome;
        left: -5px;
        position: absolute;
        top: 0;
    }

    body {
        font-family: roboto
    }

    .mosserate {
        font-family: montserrate-regular;
    }

    .link-unstyled,
    .link-unstyled:link,
    .link-unstyled:hover {
        color: inherit;
        text-decoration: inherit;
    }

    hr {
        border-bottom: 1px solid hsl(0deg 0% 80%);
        margin: 0 !important
    }

    @media (min-width: 768px) {
        .navbar-brand.abs {
            position: absolute;
            width: auto;
            left: 50%;
            transform: translateX(-50%);
            text-align: center;
        }
    }

    .header-text {
        font-family: montserrate;
        color: hsl(0deg 0% 30%);
    }

    .sub-header-text {
        font-family: roboto-light;
        color: hsl(0deg 0% 34%);
    }

    .pricing-table {
        /* background-color: #eee; */
        /* font-family: 'Montserrat', sans-serif; */
    }

    .pricing-table .block-heading {
        padding-top: 50px;
        margin-bottom: 40px;
        text-align: center;
    }

    .pricing-table .block-heading h2 {
        /* color: #3b99e0; */
    }

    .pricing-table .block-heading p {
        text-align: center;
        max-width: 420px;
        margin: auto;
        opacity: 0.7;
    }

    .pricing-table .heading {
        text-align: center;
        /* padding-bottom: 10px; */
        border-bottom: 1px solid rgba(0, 0, 0, 0.1);
    }

    .pricing-table .heading-best {
        text-align: center;
        /* padding-bottom: 10px; */
        border-bottom: 1px solid #0d6efd;
    }


    .pricing-table .item {
        background-color: #ffffff;
        /* box-shadow: 0px 2px 10px rgba(0, 0, 0, 0.075); */
        border: 1px solid #eee;
        /* padding: 30px; */
        overflow: hidden;
        position: relative;

    }

    .best-seller-item {
        /* box-shadow: 0px 2px 10px rgba(0, 0, 0, 0.075); */
        border: 1px solid #0090f0 !important;

    }


    .pricing-table .col-md-5:not(:last-child) .item {
        margin-bottom: 30px;
    }

    .pricing-table .item button {
        font-weight: 600;
    }



    .pricing-table .ribbon {
        width: 150px;
        height: 150px;
        position: absolute;
        top: -10px;
        left: -10px;
        overflow: hidden;
    }

    .pricing-table .ribbon::before,
    .pricing-table .ribbon::after {
        position: absolute;
        content: "";
        z-index: -1;
        display: block;
        border: 7px solid #4606ac;
        border-top-color: transparent;
        border-left-color: transparent;
    }

    .pricing-table .ribbon::before {
        top: 0px;
        right: 15px;
    }

    .pricing-table .ribbon::after {
        bottom: 15px;
        left: 0px;
    }

    .pricing-table .ribbon span {
        position: absolute;
        top: 30px;
        right: 0;
        transform: rotate(-45deg);
        width: 200px;
        background: hsl(150deg 100% 35%);
        padding: 10px 0;
        color: #fff;
        text-align: center;
        font-size: 17px;
        text-transform: uppercase;
        box-shadow: 0 5px 10px rgba(0, 0, 0, 0.12);
    }

    .pricing-container>.row>* {
        font-family: monospace
    }

    .pricing-table .item p {
        text-align: center;
        margin-top: 20px;
        opacity: 0.7;
    }

    .pricing-table .features .feature {
        font-weight: 600;
    }

    .pricing-table .features h4 {
        text-align: center;
        font-size: 18px;
        padding: 5px;
    }

    .pricing-table .price h4 {
        margin: 15px 0;
        font-size: 45px;
        text-align: center;
        color: #2288f9;
    }

    .pricing-table .buy-now button {
        text-align: center;
        margin: auto;
        font-weight: 600;
        padding: 9px 0;
    }

    .vertical-center {
        /* min-height: 100%; */
        /* Fallback for browsers do NOT support vh unit */
        /* min-height: 100vh; */
        /* These two lines are counted as one :-)       */

        align-items: center;
    }

    .bank {
        border: 1px solid grey;
        border-radius: 10px
    }

    .plan {
        padding-right: 0 !important;
        padding-left: 0 !important;
    }

    .best-seller-primary {
        background-color: hsl(204deg 100% 47%);
    }

    .best-seller-secondary {
        background-color: hsl(206deg 100% 44%);
    }

    .mybox {
        position: relative;
        padding: 10px 20px;
    }

    .mybox:after {
        content: '';
        position: absolute;
        left: 46.5%;
        top: 0;
        width: 7.5%;
        border-top: 5px solid #EEEEEE;
    }

    .mybox-bottom {
        position: relative;
        padding: 10px 20px;
    }

    .mybox-bottom:after {
        content: '';
        position: absolute;
        left: 46.5%;
        bottom: 0;
        width: 7.5%;
        border-bottom: 5px solid #EEEEEE;
    }

    span.bold {

        font-weight: bold;
    }

    span.dan {

        font-weight: normal !important;
    }

    .btn-pilih {
        background-color: hsl(204deg 100% 47%);
    }

    ul {
        list-style: none;
        padding: 0;
    }

    li {
        padding-left: 1.3em;
    }

    li.bawah:before {
        content: "\f058";
        /* FontAwesome Unicode */
        font-family: 'Font Awesome 5 Pro';
        display: inline-block;
        margin-left: -1.3em;
        /* same as padding-left set on li */
        width: 1.3em;
        --bs-text-opacity: 1;
        color: rgba(var(--bs-success-rgb), var(--bs-text-opacity)) !important;
        font-weight: 900;
        /* same as padding-left set on li */
    }

    .speech-bubble {
        background: #ccc;
        position: relative;
        text-align: center;
        padding: 20px 0;
        margin: 0px 30px 0px 0px;
        width: 60px;
        border-radius: 5px;
        height: 0px;
    }

    .speech-bubble:after {
        content: '';
        position: absolute;
        display: block;
        top: 50%;
        right: 100%;
        margin-top: -10px;
        width: 0;
        height: 0;
        width: 0;
        height: 0;
        border-top: 10px solid transparent;
        border-bottom: 10px solid transparent;
        border-right: 10px solid #ccc;
    }

    .cust-col {
        width: 10.67% !important;
    }

    @media (max-width: 575.98px) {
        .cust-col {
            width: 10.67% !important;
            margin-right: 1% !important;
            margin-left: 5px !important;
        }

        .social-icon {
            width: 32px
        }

        .speech-bubble {
            width: 40px;
            height: 1px;
            padding-bottom: 15px !important
        }

        .notifItem {
            margin-top: -15px !important
        }
    }

    @media (min-width: 576px) and (max-width: 767.98px) {
        .cust-col {
            margin-right: 2% !important;
            margin-left: 1.5% !important;
        }

        .social-icon {
            width: 32px
        }

        .speech-bubble {
            width: 40px;
            height: 1px;
            padding-bottom: 15px !important
        }

        .notifItem {
            margin-top: -15px !important
        }
    }

    @media (min-width: 768px) and (max-width: 991.98px) {
        .cust-col {
            margin-right: 4% !important;
            margin-left: 5% !important;
        }

        .social-icon {
            width: 32px
        }

        .speech-bubble {
            width: 40px;
            height: 1px;
            padding-bottom: 15px !important
        }

        .notifItem {
            margin-top: -15px !important
        }

    }

    @media (min-width: 992px) and (max-width: 1199.98px) {
        .cust-col {
            margin-right: 3% !important;
            margin-left: 3% !important;
        }
    }

    @media (min-width: 1200px) {
        .cust-col {
            margin-right: 1% !important;
            margin-left: 1% !important;
        }

    }

    .text-title {
        color: grey;
        opacity: 0.8;
    }

    .text-list {
        color: white;
        font-family: roboto-light;
        padding-top: 25px;
        opacity: 0.7;
    }

    .text-list>li {
        padding-left: 0px
    }

    .custom-search {
        position: relative;
        width: 300px;
    }

    .custom-search-input {
        width: 100%;
        border: 1px solid #ccc;
        border-radius: 100px;
        padding: 10px 100px 10px 20px;
        line-height: 1;
        box-sizing: border-box;
        outline: none;
        height: 50px;
    }

    .custom-search-botton {
        position: absolute;
        right: 3px;
        top: 3px;
        bottom: 3px;
        border: 0;
        background: #0090f0;
        color: #fff;
        outline: none;
        margin: 0;
        padding: 0 10px;
        border-radius: 100px;
        z-index: 2;
    }



    .icon-border {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        justify-content: center;
        width: 60px;
        height: 60px;
        border-radius: 50%;
        border: 2px solid white;
    }

</style>

<body>
    <div id="app">
        <section class="top-bar ">
            <div class="container">
                <div class="row">
                    <div class="col-xl-8 col-lg-6 col-sm-12">
                        <div class="topbar-text">
                            <span class="header-caps d-block">
                                <span class="fa-stack ">
                                    <i class='fas fa-bookmark fa-stack-2x bg-icon'
                                        style="color: hsl(204deg 100% 47%)"></i>
                                    <i class="fas fa-tag fa-stack-1x" style="color: white"></i>
                                </span>
                                <span class="pt-10" style="font-size: 11px">Gratis Ebook 9 Cara Cerdas
                                    Menggunakan Domain <a href="javascript:void(0)" class="dismis">[x]</a></span>
                            </span>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-6 d-sm-none d-none d-lg-block " style="font-size: 14px">
                        <div class="d-flex my-2 my-md-0 mr-md-3 p-2 justify-content-end"">
                            <a class="                                                   p-2 text-dark link-unstyled"
                            href="#">
                            <i class="fa fa-phone"></i>
                            0274-5305505</a>
                            <a class="p-2 text-dark link-unstyled" href="#"> <i class="fa fa-comments-alt"></i>
                                Live Chat</a>
                            <a class="p-2 text-dark link-unstyled" href="#"><i class="fa fa-user-circle"></i> Member
                                Area</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <hr>


        <section class="" style="width: 100%">
            <div class="container">
                <nav class="navbar navbar-expand-xl navbar-light ">
                    <div class="d-flex flex-grow-1">
                        <span class="w-100 d-lg-none d-block">
                            <!-- hidden spacer to center brand on mobile -->
                        </span>
                        <a class="navbar-brand d-none d-lg-inline-block" href="#">
                            <img src="{{ asset('assets/logo/logo-niagahoster-768x384.png') }}" height="55"
                                class="img-responsive" alt="Responsive image">

                        </a>
                        <a class="navbar-brand-two mx-auto d-lg-none d-inline-block" href="#">
                            <img src="{{ asset('assets/logo/logo-niagahoster-768x384.png') }}" height="30"
                                style="float: left" class="img-responsive" alt="Responsive image">
                        </a>
                        <div class="w-100 text-right">
                            <button class="navbar-toggler text-right" style="float: right" type="button"
                                data-toggle="collapse" data-target="#myNavbar">
                                <span class="navbar-toggler-icon"></span>
                            </button>
                        </div>
                    </div>
                    <div class="collapse navbar-collapse flex-grow-1 text-right justify-content-end" id="myNavbar">
                        <ul class="navbar-nav ml-auto flex-nowrap">
                            @foreach ($top_menu as $key => $menu)
                                <li class="nav-item">
                                    <a href="#"
                                        class="nav-link m-2 menu-item {{ $key == 0 ? 'nav-active' : '' }}">{{ $menu }}</a>
                                </li>
                            @endforeach

                        </ul>
                    </div>
                </nav>
            </div>
        </section>
        <hr>
        <section>
            <div class="container">
                <div class="position-relative overflow-hidden p-3 p-md-1 m-md-3 text-left ">
                    <div class="row">
                        <div class="col-lg-6 col-md-12 ">
                            <h1 class=" font-weight-normal header-text"><strong>{{ $slider['title'] }}</strong></h1>
                            <h2 class=" sub-header-text pt-3">{{ $slider['sub_title'] }}
                            </h2>
                            <div class="list-kelebihan pt-3">
                                <ul>
                                    @foreach ($slider['kelebihan'] as $kelebihan)
                                        <li class="sub-header-text bawah pt-2">{{ $kelebihan }}</li>
                                    @endforeach
                                </ul>

                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12 ">
                            <img src="{{ asset($slider['image']) }}" alt="" style="height: 90%">
                        </div>
                    </div>

                </div>
            </div>

        </section>

        <hr>
        <section>
            <div class="container marketing p-5">

                <!-- Three columns of text below the carousel -->
                <div class="row ">
                    @foreach ($engine as $engines)
                        <div class="col-lg-4 text-center p-3">
                            <div class="d-flex align-items-center vertical-center justify-content-center"
                                style="height: 80%">
                                <img src="{{ asset($engines['image']) }}" alt="Generic placeholder image" width="250"
                                    height="auto">
                            </div>
                            <div class="text-bottom pt-2" style="height: 20%">
                                <p class="p-2 sub-header-text">{{ $engines['title'] }}</p>
                            </div>
                        </div><!-- /.col-lg-4 -->
                    @endforeach
                </div><!-- /.row -->

            </div>
        </section>
        <section class="pricing-table">
            <div class="container">
                <div class="block-heading">
                    <div class="text-center">
                        <h2 class="header-text">{{ $harga['title'] }}</h2>
                        <h2 class="sub-header-text">{{ $harga['sub_title'] }}</h2>
                    </div>
                </div>
                <div class="row justify-content-md-center">
                    @foreach ($harga['plan'] as $key => $price)
                        @php
                            $best_seller = isset($price['best_seller']);
                        @endphp
                        <div class="col-md-6 col-lg-3 plan pt-1 ">
                            <div class="item  pr-3 pl-3 pb-4 {{ $best_seller ? 'best-seller-item ' : '' }}">
                                @if ($best_seller)
                                    <div class="ribbon"><span>Best Seller</span></div>

                                @endif
                                <div class="heading {{ $best_seller ? 'best-seller-primary ' : '' }}">
                                    <div @if (!$best_seller) class="heading" @endif>

                                        <h4 class="header-text" @if ($best_seller)  style="color: white" @endif>{{ ucfirst($key) }}</h4>
                                    </div>
                                    <div class="price text-center pt-2">
                                        <span style=@if ($best_seller)  "text-decoration: line-through;color: white" @else "text-decoration: line-through;opacity:0.7;" @endif class=" text-center">
                                            Rp {{ number_format($price['price'], 0, ',', '.') }}</span>
                                        @php
                                            $discount = number_format($price['discount'], 0, ',', '.');
                                            $explode = explode('.', $discount);
                                        @endphp
                                    </div>
                                    <div class="discount text-center pricing-table " style="margin-top: -15px;">
                                        <span @if ($best_seller)  style="color: white" @endif>Rp </span>
                                        <span class="header-text"
                                            style="font-size: 35pt;@if ($best_seller)  color: white @endif">{{ $explode[0] }}</span>
                                        <span @if ($best_seller)  style="color: white" @endif>.{{ $explode[1] }}</span><span  @if ($best_seller)  style="color: white" @endif>/bln</span>
                                    </div>
                                </div>

                                <div
                                    class="heading pricing-table p-3 {{ $best_seller ? 'best-seller-secondary' : '' }}">
                                    <span @if ($best_seller)  style="color: white" @endif>{{ $price['user']['value'] }}</span>
                                    <span
                                        style=" {{ $best_seller ? 'opacity: 0.7;color:white' : 'opacity: 0.5;' }}">{{ $price['user']['caption'] }}</span>
                                </div>
                                <div class="text-center pricing-table p-3">
                                    @foreach ($price['feature'] as $kunci => $feature)
                                        <div class="feature p-1">
                                            @if ($kunci == 'star')
                                                @for ($i = 0; $i < $feature['value']; $i++)
                                                    <span style="color: hsl(204deg 100% 47%)"><i
                                                            class="fa fa-star"></i></span>
                                                @endfor
                                            @else
                                                <span class="mosserate">{{ $feature['value'] }}</span>
                                                @isset($feature['caption'])
                                                    <span
                                                        {{ !isset($feature['all_bold']) ? 'style=opacity:0.5' : '' }}>{{ $feature['caption'] }}</span>
                                                @endisset
                                            @endif

                                        </div>
                                    @endforeach
                                </div>
                                <div class="text-center">
                                    <button
                                        class="btn btn-block {{ !isset($price['best_seller']) ? 'btn-outline-secondary' : 'btn-primary btn-pilih' }} rounded-pill"
                                        type="submit">Pilih Sekarang</button>

                                </div>
                            </div>
                        </div>
                    @endforeach

                </div>
            </div>
        </section>
        <section>
            <div class="container benchmark p-5">
                <div class="block-heading">
                    <div class="text-center">
                        <h2 class="sub-header-text">{{ $php_config['title'] }}</h2>
                    </div>
                </div>
                <div class="block-content">
                    @php
                        $splitList = array_chunk($php_config['list'], $php_config['limit']);
                    @endphp
                    <div class="row">
                        @foreach ($splitList as $list)
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <ul class="list-group w-100 p-3">
                                    @foreach ($list as $item)
                                        <li class="list-group-item disabled w-100">
                                            <div class="items row">
                                                <div class="col-1">
                                                    <i class="fa fa-check-circle text-success w-10"></i>
                                                </div>
                                                <div class="col-11 text-center">
                                                    <span>{{ $item }}</span>

                                                </div>
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </section>
        <section>
            <div class="container mybox ">
                <div class="block-heading pt-4">
                    <div class="text-center">
                        <h2 class="sub-header-text">{{ $whatYouGet['title'] }}</h2>
                    </div>
                </div>
                <div class="block-content">
                    <div class="row">
                        @foreach ($whatYouGet['list'] as $termasuk)
                            <div class="col-lg-4 col-md-3 col-sm-6 text-center p-4">
                                <div class="d-flex align-items-center vertical-center justify-content-center">
                                    <img src="{{ asset($termasuk['image']) }}" alt="Generic placeholder image"
                                        width="75" height="auto">
                                </div>
                                <div class="text-bottom pt-2 d-flex justify-content-center">
                                    <div class="w-75">
                                        <p class="header-text">{{ $termasuk['title'] }}</p>
                                        <p class="sub-header-text">{{ $termasuk['sub_title'] }}</p>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>

                </div>
                <div class="mybox-bottom"></div>
            </div>
        </section>
        <section>
            <div class="container">

                <div class="block-heading pt-4">
                    <div class="text-center">
                        <h2 class="sub-header-text">{{ $dukungan['title'] }}</h2>
                    </div>
                </div>
                <div class="block-content">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <h4 class=" sub-header-text pt-3">{{ $dukungan['caption'] }}</h4>
                            <div class="keunggulan p-3">
                                @foreach ($dukungan['list'] as $dukungans)
                                    <p class="sub-header-text" style="font-size: 16px">
                                        <i class="fa fa-check-circle text-success"></i>
                                        @php

                                            $bold_text = $parsed = get_string_between($dukungans, '[', ']');
                                            $formatedText = '<span class="bold">' . $bold_text . '</span>';
                                            $replaced = replace_all_text_between($dukungans, '[', ']', $formatedText);
                                        @endphp
                                        <span>{!! $replaced !!}</span>
                                    </p>
                                @endforeach
                                <p class="sub-header-text" style="font-size: 14px">{{ $dukungan['nb'] }}</p>
                                <div class="pt-4">
                                    <button class="btn btn-block btn-primary rounded-pill btn-pilih"
                                        type="submit">{{ $dukungan['button'] }}</button>

                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <img src="{{ asset($dukungan['image']) }}" alt="" style="height: 90%">

                        </div>
                    </div>
                </div>
            </div>
            <hr>
        </section>
        <section>
            <div class="container modul">
                <div class="block-heading pt-4">
                    <div class="text-center">
                        <h2 class="sub-header-text">{{ $modul['title'] }}</h2>
                    </div>
                </div>
                @php
                    $splitListModul = array_chunk($modul['list'], $modul['limit']);
                @endphp
                <div class="block-content pt-5 pb-5">
                    <div class="row">
                        @foreach ($splitListModul as $modulList)
                            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-4 col-6  ">
                                <ul class="list-group w-100 p-3">
                                    @foreach ($modulList as $modulItem)
                                        <li class="sub-header-text">
                                            <span>{{ $modulItem }}</span>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        @endforeach
                    </div>
                    <div class="text-center">
                        <button class="btn btn-block btn-outline-secondary rounded-pill"
                            type="submit">{{ $modul['button'] }}</button>
                    </div>
                </div>
            </div>
        </section>
        <section>
            <div class="container cs">

                <div class="block-content pt-5 pb-5">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <h1 class="sub-header-text">{{ $cs['title'] }}</h1>
                            <p class="sub-header-text">
                                {!! $cs['caption'] !!}
                            </p>
                            <div class="pt-4">
                                <button class="btn btn-lg btn-block btn-primary rounded-pill btn-pilih"
                                    type="submit">{{ $cs['button'] }}</button>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <img src="{{ asset($cs['image']) }}" alt="" style="height: 100%">

                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section style="background-color: hsl(0deg 0% 97%)">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12 pt-3 pb-3">
                        <span class="montserrate-light" style="opacity: 0.7;font-size:14pt">Bagikan jika Anda menyukai
                            halaman ini.</span>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 pt-3 pb-3">

                        <div class="row">
                            <div class="col-1"><img class="social-icon "
                                    src="{{ asset('assets/logo/facebook.png') }}" width="42px" alt=""></div>
                            <div class="cust-col">
                                <div class="speech-bubble">
                                    <div class="notifItem" style="top: -9px;margin-top: -10px;">33
                                    </div>
                                </div>
                            </div>

                            <div class="col-1"><img class="social-icon "
                                    src="{{ asset('assets/logo/twitter.png') }}" width="42px" alt=""></div>
                            <div class="cust-col">
                                <div class="speech-bubble">
                                    <div class="notifItem" style="top: -9px;margin-top: -10px;">33
                                    </div>
                                </div>
                            </div>

                            <div class="col-1"><img class="social-icon "
                                    src="{{ asset('assets/logo/google-plus.png') }}" width="42px" alt=""></div>
                            <div class="cust-col">
                                <div class="speech-bubble">
                                    <div class="notifItem" style="top: -9px;margin-top: -10px;">33
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section style="background-color: hsl(204deg 100% 47%)">
            <div class="container pt-5 pb-5">
                <div class="row">
                    <div class="col-lg-8 col-md-12 col-sm-12" style="    border-right: 1px solid rgba(234, 234, 234, 0.5);
                    -webkit-background-clip: padding-box; /* for Safari */
                    background-clip: padding-box; /* for IE9+, Firefox 4+, Opera, Chrome */">
                        <span class="sub-header-text" style="color:white;font-size:26pt">Perlu </span>
                        <span class="mosserate" style="color:white;font-size: 26pt">BANTUAN? </span>
                        <span class="sub-header-text" style="color:white;font-size:26pt">Hubungi Kami : </span>
                        <span class="mosserate" style="color:white;font-size: 26pt">0275-5305505 </span>
                    </div>
                    <div class="col-lg-4 col-md-12 text-center">
                        <button class="btn btn-lg btn-block btn-outline-light  rounded-pill w-50" type="submit"><i
                                class="fa fa-comments-alt fa-1x"></i> Live Chat</button>
                    </div>
                </div>

            </div>
        </section>
        <section style="background-color: hsl(0deg 0% 19%) " class="pt-3">
            <div class="container footer pt-5">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 col-6 pt-3">
                        <span class="mosserate text-title">HUBUNGI KAMI</span>
                        <ul class="text-list">
                            <li>0274-5305505</li>
                            <li>Senin - Minggu</li>
                            <li>24 Jam Nonstop</li>
                            <li class="pt-5">Jl. Selokan Mataram Monjali</li>
                            <li>Karang Jati MT I/304</li>
                            <li>Sinduadi, Mlati, Sleman</li>
                            <li>Yogyakarta 55284</li>
                        </ul>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 col-6 pt-3">
                        <span class="mosserate text-title">Layanan</span>
                        <ul class="text-list">
                            <li>Domain</li>
                            <li>Shared Hosting</li>
                            <li>Cloud VPS Hosting</li>
                            <li>Managed VPS Hosting</li>
                            <li>Web Builder</li>
                            <li>Keamanan SSL / HTTPS</li>
                            <li>Jasa Pembuatan Website</li>
                            <li>Program Affiliasi</li>
                        </ul>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 col-6 pt-3">
                        <span class="mosserate text-title">Service Hosting</span>
                        <ul class="text-list">
                            <li>Hosting Murah</li>
                            <li>Hosting Indonesia</li>
                            <li>Hosting Singapura SG</li>
                            <li>Hosting PHP</li>
                            <li>Hosting Wordpress</li>
                            <li>Hosting Laravel</li>
                        </ul>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 col-6 pt-3">
                        <span class="mosserate text-title">Tutorial</span>
                        <ul class="text-list">
                            <li>Knoledgebase</li>
                            <li>Blog</li>
                            <li>Cara Pembayaran</li>
                        </ul>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 col-6 pt-3">
                        <span class="mosserate text-title">Tentang Kami</span>
                        <ul class="text-list">
                            <li>Tim Niagahoster</li>
                            <li>Karir</li>
                            <li>Events</li>
                            <li>Penawaran & Promo Spesian</li>
                            <li>Kontak Kami</li>
                        </ul>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 col-6 pt-3">
                        <span class="mosserate text-title">Kenapa pilih Niagahoster</span>
                        <ul class="text-list">
                            <li>Support Terbaik</li>
                            <li>Garansi Harga Termurah</li>
                            <li>Domain Gratis Selamanya</li>
                            <li>Datacenter Hosting Terbaik</li>
                            <li>Review Pelanggan</li>
                        </ul>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col-12 pt-3">
                        <span class="mosserate text-title">Newsletter</span>
                        <div class="row">
                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 col-12">
                                <div class="newsletter pt-3">
                                    <div class="custom-search w-100">
                                        <input type="text" class="custom-search-input" placeholder="Email">

                                        <button class="custom-search-botton" type="submit">Berlangganan</button>
                                    </div>
                                    <div class="pt-2">
                                        <span class="text-list " style="opacity: 0.5">Dapatkan promo dan konten
                                            menarik dari penyedia hosting terbaik anda</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 col-12 pt-5 d-flex">
                                <div class="row w-100 justify-content-center">
                                    <div class="col-4">
                                        <div class="icon-border text-center">
                                            <svg width="11" height="20" viewBox="0 0 11 20" fill="none"
                                                xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M9.11999 3.32003H11V0.14003C10.0897 0.045377 9.17514 -0.00135428 8.25999 2.98641e-05C5.53999 2.98641e-05 3.67999 1.66003 3.67999 4.70003V7.32003H0.609985V10.88H3.67999V20H7.35998V10.88H10.42L10.88 7.32003H7.35998V5.05003C7.35998 4.00003 7.63999 3.32003 9.11999 3.32003Z"
                                                    fill="white"></path>
                                            </svg>
                                        </div>

                                    </div>
                                    <div class="col-4">
                                        <div class="icon-border text-center">
                                            <i class="fab fa-twitter p-2" style="font-size: 32px;color: white"></i>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="icon-border text-center">
                                            <img src="{{ asset('assets/images/g-plus.svg') }}" alt=""
                                                style="filter: invert(94%) sepia(100%) saturate(18%) hue-rotate(277deg) brightness(105%) contrast(105%);">
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>

                    </div>
                    <div class="col-12 pt-3">
                        <span class="mosserate text-title">Pembayaran</span>
                        <div class="row pt-3">
                            <div class="col-xl-1 col-lg-2 col-md-3 col-sm-4 col-xs-4 col-6 text-center">
                                <div class="bank">
                                    <img src="{{ asset('assets/images/bca.png') }}" height="40px" alt="">
                                </div>
                            </div>
                            <div class="col-xl-1 col-lg-2 col-md-3 col-sm-4 col-xs-4 col-6 text-center">
                                <div class="bank">
                                    <img src="{{ asset('assets/images/mandiri.png') }}" height="40px" alt="">
                                </div>
                            </div>
                            <div class="col-xl-1 col-lg-2 col-md-3 col-sm-4 col-xs-4 col-6 text-center">
                                <div class="bank">
                                    <img src="{{ asset('assets/images/bni.png') }}" height="40px" alt="">
                                </div>
                            </div>
                            <div class="col-xl-1 col-lg-2 col-md-3 col-sm-4 col-xs-4 col-6 text-center">
                                <div class="bank">
                                    <img src="{{ asset('assets/images/visa.png') }}" height="40px" alt="">
                                </div>
                            </div>
                            <div class="col-xl-1 col-lg-2 col-md-3 col-sm-4 col-xs-4 col-6 text-center">
                                <div class="bank">
                                    <img src="{{ asset('assets/images/mastercard.png') }}" height="40px" alt="">
                                </div>
                            </div>
                            <div class="col-xl-1 col-lg-2 col-md-3 col-sm-4 col-xs-4 col-6 text-center">
                                <div class="bank">
                                    <img src="{{ asset('assets/images/atmbersama.png') }}" height="40px" alt="">
                                </div>
                            </div>
                            <div class="col-xl-1 col-lg-2 col-md-3 col-sm-4 col-xs-4 col-6 text-center">
                                <div class="bank">
                                    <img src="{{ asset('assets/images/permata.png') }}" height="40px" alt="">
                                </div>
                            </div>
                            <div class="col-xl-1 col-lg-2 col-md-3 col-sm-4 col-xs-4 col-6 text-center">
                                <div class="bank">
                                    <img src="{{ asset('assets/images/prima.png') }}" height="40px" alt="">
                                </div>
                            </div>
                            <div class="col-xl-1 col-lg-2 col-md-3 col-sm-4 col-xs-4 col-6 text-center">
                                <div class="bank">
                                    <img src="{{ asset('assets/images/alto.png') }}" height="40px" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="text-footer">
                        <p class="text-list " style="opacity: 0.5">Aktivasi instant dengan e-Payment, Hosting dan
                            domain langsung aktif!</p>
                    </div>
                    <hr style="color: black!important">
                    <div class="row">
                        <div class="col-8">
                            <span class="text-list" style="opacity: 0,5;font-size:11px">Copyright 2016 Niaga
                                Hoster | Hosting powared by PHP7,CloudLinux,BitNinja and DC Biznet Technovillage
                                Jakarta</span>
                            <span class="text-list" style="opacity: 0,5;font-size:11px">Cloud VPS Murah powared by
                                Webuzo Softculous, Intel SSD and cloud computing technology</span>
                        </div>
                        <div class="col-4 text-right">
                            <span class="text-list" style="opacity: 0,5;font-size:11px;float:right">Syarat
                                Ketentuan | Kebijakan Privasi</span>

                        </div>
                    </div>


                </div>

            </div>
        </section>
    </div>

    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script>
        $('.dismis').click(function() {
            $('.header-caps').removeClass('d-block').addClass('d-none')
        })
    </script>
</body>

</html>
