<?php
function get_string_between($string, $start, $end)
{
    $string = ' ' . $string;
    $ini = strpos($string, $start);
    if ($ini == 0) {
        return '';
    }
    $ini += strlen($start);
    $len = strpos($string, $end, $ini) - $ini;
    return substr($string, $ini, $len);
}

function replace_all_text_between($str, $start, $end, $replacement)
{
    $replacement = $start . $replacement . $end;
    $start = preg_quote($start, '/');
    $end = preg_quote($end, '/');
    $regex = "/({$start})(.*?)({$end})/";
    $res =  preg_replace($regex, $replacement, $str);
    $res = str_replace('[', '', $res);
    $res = str_replace(']', '', $res);
    return $res;
}
