<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $harga = [
        'title' => 'Paket Hosting Singapura Yang Tepat',
        'sub_title' => 'Diskon 40% + Domain dan SSL Gratis untuk Anda',
        'plan' => [
            'bayi'  => [
                'discount' => 14900,
                'price' => 19900,
                'user' => [
                    'value' => 938,
                    'caption' => 'Pengguna Terdaftar'
                ],
                'feature' => [
                    'power' => [
                        'value' => '0.5X',
                        'caption' => 'Resource Power',
                        'all_bold' => true
                    ],
                    'disk_space' => [
                        'value' => '500 MB',
                        'caption' => 'Disk Space'
                    ],
                    'bandwidth' => [
                        'value' => 'Unlimited',
                        'caption' => 'Bandwidth'
                    ],
                    'databases' => [
                        'value' => 'Unlimited',
                        'caption' => 'Databases'
                    ],
                    'domain' => [
                        'value' => 1,
                        'caption' => 'Domain'
                    ],
                    'ssl' => [
                        'value' => 'Unlimited SSL',
                        'caption' => 'Gratis Selamanya'
                    ],
                ],
            ],
            'pelajar'  => [
                'discount' => 23450,
                'price' => 46900,
                'user' => [
                    'value' => 4168,
                    'caption' => 'Pengguna Terdaftar'
                ],
                'feature' => [

                    'power' => [
                        'value' => '1X',
                        'caption' => 'Resource Power',
                        'all_bold' => true
                    ],
                    'disk_space' => [
                        'value' => 'Unlimited',
                        'caption' => 'Disk Space'
                    ],
                    'bandwidth' => [
                        'value' => 'Unlimited',
                        'caption' => 'Bandwidth'
                    ],
                    'mail' => [
                        'value' => 'Unlimited',
                        'caption' => 'POP3 Email'
                    ],
                    'databases' => [
                        'value' => 'Unlimited',
                        'caption' => 'Databases'
                    ],
                    'domain' => [
                        'value' => 10,
                        'caption' => 'Domain'
                    ],
                    'ssl' => [
                        'value' => 'Unlimited SSL',
                        'caption' => 'Gratis Selamanya'
                    ],
                ]

            ],
            'personal'  => [
                'discount' => 38900,
                'price' => 58900,
                'best_seller' => true,
                'user' => [
                    'value' => 10017,
                    'caption' => 'Pengguna Terdaftar'
                ],
                'feature' => [

                    'power' => [
                        'value' => '2X',
                        'caption' => 'Resource Power',
                        'all_bold' => true
                    ],
                    'disk_space' => [
                        'value' => 'Unlimited',
                        'caption' => 'Disk Space'
                    ],
                    'bandwidth' => [
                        'value' => 'Unlimited',
                        'caption' => 'Bandwidth'
                    ],
                    'mail' => [
                        'value' => 'Unlimited',
                        'caption' => 'POP3 Email'
                    ],
                    'databases' => [
                        'value' => 'Unlimited',
                        'caption' => 'Databases'
                    ],
                    'addon_domain' => [
                        'value' => 'Unlimited',
                        'caption' => 'Addon Domain'
                    ],
                    'backup' => [
                        'value' => 'Instant',
                        'caption' => 'Backup'
                    ],

                    'domain' => [
                        'value' => 10,
                        'caption' => 'Domain'
                    ],
                    'ssl' => [
                        'value' => 'Unlimited SSL',
                        'caption' => 'Gratis Selamanya'
                    ],
                    'ns' => [
                        'value' => 'Private',
                        'caption' => 'Name Server'
                    ],
                    'spam_assasin' => [
                        'value' => 'Spam Assasin',
                        'caption' => 'Mail Protection'
                    ],


                ]
            ],
            'bisnis'  => [
                'discount' => 65900,
                'price' => 109900,
                'user' => [
                    'value' => 35532,
                    'caption' => 'Pengguna Terdaftar'
                ],
                'feature' => [
                    'power' => [
                        'value' => '2X',
                        'caption' => 'Resource Power',
                        'all_bold' => true
                    ],
                    'disk_space' => [
                        'value' => 'Unlimited',
                        'caption' => 'Disk Space'
                    ],
                    'bandwidth' => [
                        'value' => 'Unlimited',
                        'caption' => 'Bandwidth'
                    ],
                    'mail' => [
                        'value' => 'Unlimited',
                        'caption' => 'POP3 Email'
                    ],
                    'databases' => [
                        'value' => 'Unlimited',
                        'caption' => 'Databases'
                    ],
                    'addon_domain' => [
                        'value' => 'Unlimited',
                        'caption' => 'Addon Domain'
                    ],
                    'backup' => [
                        'value' => 'Instant',
                        'caption' => 'Backup'
                    ],

                    'domain' => [
                        'value' => 10,
                        'caption' => 'Domain'
                    ],
                    'ssl' => [
                        'value' => 'Unlimited SSL',
                        'caption' => 'Gratis Selamanya'
                    ],
                    'ns' => [
                        'value' => 'Private',
                        'caption' => 'Name Server'
                    ],
                    'support' => [
                        'value' => 'Prioritas',
                        'caption' => 'Layanan Support'
                    ],
                    'star' => [
                        'value' => 5
                    ],
                    'spam_expert' => [
                        'value' => 'SpamExpert',
                        'caption' => 'Pro Mail Protection'
                    ],


                ]
            ],
        ]
    ];

    $engine = [
        [
            'title' => 'PHP Zend Guard Loader',
            'image' => 'assets/svg/illustration-banner-PHP-zenguard01.svg'
        ],
        [
            'title' => 'PHP Composser',
            'image' => 'assets/svg/icon-composer.svg'
        ],
        [
            'title' => 'PHP IonCube Loader',
            'image' => 'assets/svg/icon-php-hosting-ioncube.svg'
        ],

    ];

    $slider = [
        'title' => 'PHP Hosting',
        'sub_title' => 'Cepat, Handal, penuh dengan modul PHP yang anda butuhkan.',
        'image' => 'assets/svg/illustration banner PHP hosting-01.svg',
        'kelebihan' => [
            'Solusi PHP untuk peforma query lebih cepat.',
            'Konsumsi memory yang lebih rendah.',
            'Support PHP 5.3, PHP 5.4, PHP 5.5, PHP 5.6, PHP 7.',
            'Fitur enskripsi IonCube dan ZendGuard Loaders.'
        ]
    ];

    $top_menu = [
        'Hosting', 'Domain', 'Server', 'Website', 'Afiliasi', 'Promo', 'Pembayaran', 'Review', 'Kontak', 'Blog'
    ];

    $php_config = [
        'title' => 'Powerful dengan limit php yang lebih besar',
        'limit' => 3,
        'list' => [
            'max execution time 300s',
            'max execution time 300s',
            'PHP memory limit 1024 MB',
            'Post max size 128 MB',
            'Upload max filesize 128 MB',
            'Max input vars 25000',
        ]
    ];

    $whatYouGet = [
        'title' => 'Semua paket hosting sudah termasuk',
        'per_row' => 3,
        'list' => [
            [
                'title' => 'PHP semua versi',
                'sub_title' => 'Pilih mulai dari versi PHP 5.3 s/d PHP 7. Ubah sesuka anda.',
                'image' => 'assets/svg/icon PHP Hosting_PHP Semua Versi.svg'
            ],
            [
                'title' => 'MySQL Versi 5.6',
                'sub_title' => 'Nikmati MySQL versi terbaru, tercepat dan kaya akan fitur.',
                'image' => 'assets/svg/icon PHP Hosting_My SQL.svg'
            ],
            [
                'title' => 'Panel Hosting cPanel',
                'sub_title' => 'Kelola website dengan panel canggih yang familiar di hati anda.',
                'image' => 'assets/svg/icon PHP Hosting_CPanel.svg'
            ],
            [
                'title' => 'Garansi Uptime 99,99%',
                'sub_title' => 'Data center yang mendukung kelangsungan website Anda 24/7',
                'image' => 'assets/svg/icon PHP Hosting_garansi uptime.svg'
            ],
            [
                'title' => 'Database InnoDB Unlimited',
                'sub_title' => 'Jumlah dan ukuran database yang tumbuh sesuai kebutuhan Anda',
                'image' => 'assets/svg/icon PHP Hosting_InnoDB.svg'
            ],
            [
                'title' => 'Wildcard Remote MySQL',
                'sub_title' => 'Mendukuk s/d 25 max_user_connections dan 100 max_connections',
                'image' => 'assets/svg/icon PHP Hosting_My SQL remote.svg'
            ],
        ]
    ];

    $dukungan = [
        'title' => 'Mendukung Penguh Framework Laravel',
        'caption' => 'Tak perlu menggunakan dedicated server atau VPS yang mahal. Layanan PHP hosting murah kami mendukung penuh framework favorite Anda',
        'image' => 'assets/svg/illustration banner support laravel hosting.svg',
        'nb' => 'Nb. Composer dan SSH hanya tersedia pada paket Personal dan Bisnis',
        'button' => 'Pilih Hosting Anda',
        'list' => [
            'Install Laravel [1 klik] dengan Softaculous Installer',
            'Mendukung enskripsi [PHP MCrypt, phar, mbstring, json <span class="dan"> dan </span> fileinfo]',
            'Tersedia [Composer <span class="dan">dan</span> SSH] untuk menginstall package pilihan Anda'
        ]
    ];

    $modul = [
        'title' => 'Modul Lengkap untuk Menjalankan Aplikasi PHP Anda',
        'limit' => 15,
        'button' => 'Selengkapnya',
        'list' => [
            'IcePHP', 'apc', 'apcu', 'apm', 'ares', 'bcmath', 'bcompiler', 'big_int', 'bigset', 'bloomy', 'bz2_filter', 'clamav', 'coin_acceptor', 'crack', 'dba', 'http', 'huffman', 'idn', 'igbinary', 'imagic', 'imap', 'included', 'inotify', 'interbase', 'intl', 'ioncube_loader', 'ioncube_loader_4', 'jsmin', 'json', 'ldap', 'nd_pdo_mysql', 'oauth', 'oci8', 'odbc', 'opcache', 'pdf', 'pdo', 'pdo_dblib', 'pdo_fiirebird', 'pdo_odbc', 'pdo_pgsql', 'pdo_sqlite', 'pgsql', 'phalcon', 'stats', 'stem', 'stomp', 'suhosin', 'sybase_ct', 'sysvmsg', 'stsvsem', 'sysvshm', 'tidy', 'timezonedb', 'trader', 'translit', 'uploadprogress', 'uri_template', 'uuid'
        ]
    ];

    $cs = [
        'title' => 'Linux Hosting yang Stabil dengan Teknologi LVE',
        'caption' => 'SuperMicro <span class="bold">Intel Xeon 24-Cores</span> dengan RAM <span class="bold">128 GB</span> dan teknologi <span class="bold">LVE Cloud Linux</span> untuk stabilitas server Anda. Dilengkapi dengan <span class="bold">SSD</span> untuk kecepatan <span class="bold">MySQL</span> dan cahing. Apache load balancer berbasis LiteSpeed Technologies, <span class="bold">CageFS</span> security, <span class="bold">Raid-10</span> protection dan auto backup untuk keamanan website PHP Anda.',
        'button' => 'Pilih Hosting Anda',
        'image' => 'assets/images/Image support.png'
    ];
    return view('index', compact('harga', 'engine', 'slider', 'top_menu', 'php_config', 'whatYouGet', 'dukungan', 'modul','cs'));
});
